requirejs.config({
    baseUrl: 'js',
    paths: {
      app: 'app',
      lib: 'lib',
      main: 'app/main',
      globals: 'app/globals',
      tasks: 'app/tasks',
      taskManager: 'app/taskManager',
      cookie: 'app/cookie'
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['app/main']);