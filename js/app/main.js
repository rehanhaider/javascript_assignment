define(['taskManager'], function (taskManager) {
    'use strict';
    taskManager.init();
});