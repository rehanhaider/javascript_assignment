define(['globals'],function (globals) {
    'use strict';

    function Task(description,id,creationTime,status,updateData){
        this.id = id !== undefined? id : ++globals.incrementelId;
        this.description = description;
        this.creationTime = creationTime !== undefined? creationTime : new Date().getTime();
        this.status = status !== undefined? status : 'pending';
        this.updateData = updateData;

        this.markComplete = function(){
            if(this.status !== 'complete'){
                var el = document.getElementById('task' + this.id);
                var taskSatus = el.getElementsByClassName('task-status');
                taskSatus[0].classList.remove('pending');
                taskSatus[0].classList.add('complete');

                var checkBox = el.getElementsByClassName('task-check-box');

                el.removeChild(checkBox[0]);

                //$('#task' + this.id + ' > .task-status').removeClass('pending').addClass('complete');
                //$('#task' + this.id + ' > .task-check-box').remove();

                this.status = 'complete';
                if(globals.dataLoaded){
                    updateData();
                }
                if(this.timeEvent !== undefined && this.timeEvent !== null){
                    clearTimeout(this.timeEvent);
                }
            }
        }
    }

    function SimpleTask(description,id,creationTime,status,updateData){
        Task.call(this,description,id,creationTime,status,updateData);
        
        this.getData = function (){
            return {
                id: this.id,
                description: this.description,
                type: 'simpleTask',
                creationTime: this.creationTime,
                status: this.status
            }
        }
        
        this.populate = function (){
            globals.divTasks.innerHTML = globals.divTasks.innerHTML +
                '<div class="box simpleTask" id="task' + this.id + '">' +
                    '<div class="task-description">' + this.description + '</div>' +
                    (this.status === 'pending' ? '<div class="task-check-box"><input type="checkbox" class="mark-as-complete"/></div>' : '') +
                    '<div class="task-status ' + this.status + '"></div>' +
                '</div>';
        }
        
        this.populate();
    }

    function TimeBasedTask(description,id,creationTime,status,updateData){
        Task.call(this,description,id,creationTime,status,updateData);
        
        this.completionTime = this.creationTime + globals.reminderTime;
        
        var currentTime = new Date().getTime();
        if(this.status !== 'complete' && this.completionTime - 10 <= currentTime){
            this.status = 'complete';
        }
        
        this.getData = function (){
            return {
                id: this.id,
                description: this.description,
                type: 'timeBased',
                creationTime: this.creationTime,
                status: this.status
            }
        }
        
        this.populate = function (){
            globals.divTasks.innerHTML = globals.divTasks.innerHTML +
                '<div class="box timeBased" id="task' + this.id + '">' +
                    '<div class="task-description">' + this.description + '</div>' +
                    (this.status === 'pending' ? '<div class="task-check-box"><input type="checkbox" class="mark-as-complete"/></div>' : '') +
                    '<div class="task-status ' + this.status + '"></div>' +
                '</div>';
        }
        
        this.populate();

        if(this.status !== 'complete' && this.completionTime > new Date().getTime()){
            this.timeEvent = setTimeout(
                function(context){
                    return function(){
                        context.markComplete();
                    }
                }(this),
                this.completionTime - new Date().getTime()
            );
        }
    }

    function ReminderTask(description,id,creationTime,status,updateData){
        Task.call(this,description,id,creationTime,status,updateData);
        
        this.completionTime = this.creationTime + globals.reminderTime;
        
        this.getData = function (){
            return {
                id: this.id,
                description: this.description,
                type: 'reminder',
                creationTime: this.creationTime,
                status: this.status
            }
        }
        
        this.populate = function (){
            var el = '<div class="box reminder" id="task' + this.id + '">' +
                    '<div class="task-description">' + this.description + '</div>' +
                    (this.status === 'pending' ? '<div class="task-check-box"><input type="checkbox" class="mark-as-complete"/></div>' : '') +
                    '<div class="task-status ' + this.status + '"></div>' +
                    '</div>';
            
            if(this.status !== 'complete' && this.completionTime - 10 <= new Date().getTime()){
                //globals.divTasks.prepend(el);
                globals.divTasks.innerHTML = el + globals.divTasks.innerHTML;
            } else {
                //Globals.divTasks.append(el);
                globals.divTasks.innerHTML = globals.divTasks.innerHTML + el;
            }
        }
        
        this.populate();
        
        if(this.status !== 'complete' && this.completionTime > new Date().getTime()){
            this.timeEvent = setTimeout(
                function(context){
                    return function(){
                        if(context.status !== 'complete'){
                            globals.divTasks.removeChild(document.getElementById('task' + context.id));
                            //$('#task' + context.id).remove();
                            context.populate();
                        }
                    }
                }(this),
                this.completionTime - new Date().getTime()
            );
        }
    }

    return {
        SimpleTask : SimpleTask,
        TimeBasedTask : TimeBasedTask,
        ReminderTask : ReminderTask
    };
});