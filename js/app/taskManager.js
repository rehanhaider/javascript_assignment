define(['globals','tasks','cookie'],function (globals,taskClasses,cookie) {
    'use strict';

	var tasks = [],
		sortedTasks = [];
		
	var btnAddTask,
		inputDescription,
		selectType;
	
	function initialize(){
		bindEvents();
		loadData();
	}
	
	function bindEvents(){
		btnAddTask = document.getElementById('addTask');
		//btnAddTask = $('#addTask');
		inputDescription = document.getElementById('description');
		//inputDescription = $('#description');
		selectType = document.getElementById('type');
		//selectType = $('#type');
		globals.divTasks = document.getElementById('tasks');
		//globals.divTasks = $('#tasks');
		
		//btnAddTask.click(function(event){
		btnAddTask.addEventListener("click", function(event){
			event.preventDefault();
			var description = inputDescription.value;
			//var description = inputDescription.val();
			var type = selectType.value;
			//var type = selectType.val();
			
			if (description === undefined || description === null || description === ''){
				window.alert('Please provide some description for task!');
			} else if (type === undefined || type === null || type === ''){
				window.alert('Please select task type!');
			} else {
				addTask(description, type);
				updateData();
				document.getElementsByTagName('form')[0].reset();
				//$(this).closest('form')[0].reset();
			}
		});
/*
		$('body').on('click', '.mark-as-complete', function() {
			var parent = $(this).parent().parent();
			var id = parent.attr('id');
			id = id.replace('task','') - 1;
			tasks[id].markComplete();
		});
*/
		globals.divTasks.addEventListener('click',function(e){
			if(e.target && e.target.classList == 'mark-as-complete'){//do something
				var parent = e.target.parentElement.parentElement;
				var id = parent.getAttribute('id');
				id = id.replace('task','') - 1;
				tasks[id].markComplete();
		 	}
		 });
	}
	
	function addTask(description, type, id, creationTime, status){
        var task = null;
		switch (type) {
			case 'simpleTask':
				task = new taskClasses.SimpleTask(description,id,creationTime,status,updateData);
				break;
			case 'timeBased':
				task = new taskClasses.TimeBasedTask(description,id,creationTime,status,updateData);
				break;
			case 'reminder':
				task = new taskClasses.ReminderTask(description,id,creationTime,status,updateData);
				break;
		}
		
		if(task !== null){
			tasks.push(task);
		}
	}
	
	function loadData(){
		var temp;
		if (typeof(Storage) !== "undefined") {
			temp = localStorage.getItem("tasks");
		} else {
			temp = cookie.read("tasks");
		}
		
		temp = JSON.parse(temp);
		
		if(temp !== null){
			var length = temp.length;
			for(var i=0; i< length; ++i){
				addTask(temp[i].description,temp[i].type,temp[i].id,temp[i].creationTime,temp[i].status);
			}
			globals.incrementelId = length;
		}
		
		updateData();
		globals.dataLoaded = true;
	}
	
	function updateData(){
		var data = null,
			temp = [],
			length = tasks.length;
		
		for(var i=0; i< length; ++i){
			temp.push(tasks[i].getData());
		}
		
		data = JSON.stringify(temp);
		
		if (typeof(Storage) !== "undefined") {
			localStorage.setItem("tasks", data);
		} else {
			cookie.create("tasks", data,50);
		}
	}
	
	return {
		init: initialize,
		updateData: updateData
	};
});