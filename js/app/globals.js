define(function () {
    'use strict';
    var incrementelId = 0,
        reminderTime = 20000,
        divTasks = null,
        dataLoaded = false;

    return {
        incrementelId : incrementelId,
        reminderTime : reminderTime,
        divTasks : divTasks,
        dataLoaded : dataLoaded
    };
});